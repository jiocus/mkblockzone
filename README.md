# Domain blocking with bind9 response policy zones

## Installation
### Debian-based
`sudo apt install bind9 wget`

### Run **generate-zonefile.sh**
1. `chmod -x generate-zonefile.sh`
2. `sudo ./generate-zonefile.sh`

### Edit `/etc/bind/named.conf`
This example configuration bundles `named.conf.options` and other *includes* directly in `named.conf`.
```bash
options {
    directory "/var/cache/bind";

    recursion yes;
    dnssec-validation auto;
    auth-nxdomain no;

    listen-on port 53 {
        10.1.2.3;
    };

    listen-on-v6 port 53 {
        fc90:fbd8:4093:fe13:3359:f8fc:7644:e1c2;
    };

    forwarders {
        8.26.56.26;
        8.20.247.20;
        1.1.1.1;
        1.0.0.1;
    };

    response-policy {
        zone "allowed";
        zone "blocked";
        zone "hosts";
    };
};

//include "/etc/bind/zones.rfc1918";

zone "allowed" {
    type master;
    file "/etc/bind/db.allowed";
    allow-query { localhost; };
};

zone "blocked" {
    type master;
    file "/etc/bind/db.blocked";
    allow-query { localhost; };
};

zone "hosts" {
    type master;
    file "/etc/bind/db.hosts";
    allow-query { localhost; };
};


include "/etc/bind/named.conf.default-zones";
```

## Optional Zones
Two additional zones, *allowed* and *blocked*, are created besides *hosts*. Manual editing is required for these zones to have effect, i.e. no domains have been added during the initial creation.

### Allowed
Resources added to `db.allowed` with a *CNAME* pointing to *rpz-passthru.* will override any other policy which may block that resource. This guarantees that domains added here will be accessible regardless. This zone is not updated by the script after creation.
```
; Allow resources with RPZ passthru bypass.
;
$TTL	1H
@	IN	SOA	localhost. root.localhost. (
                        1 ; Serial
                       1H ; Refresh
                      30m ; Retry
                      28d ; Expire
                    24H ) ; Negative Cache TTL
;
@	IN	NS	localhost.
;
www.w3c.org       CNAME  rpz-passthru.
*.dev             CNAME  rpz-passthru.
```
Zone statement in `named.conf`:
```
zone "allowed" {
    type master;
    file "/etc/bind/db.allowed";
    allow-query { localhost; };
};
```

### Blocked
Resources added to `db.blocked` with a *CNAME* pointing to *.* will disable access to that resource. Domains added here will be blocked regardless of domain updates to *db.hosts*. This zone is not updated by the script after creation.

```
; Persistent custom blocked resources.
;
$TTL	1H
@	IN	SOA	localhost. root.localhost. (
                        1 ; Serial
                       1H ; Refresh
                      30m ; Retry
                      28d ; Expire
                    24H ) ; Negative Cache TTL
;
@	IN	NS	localhost.
;
*.gov             CNAME  .
```

Zone statement in `named.conf`:
```
zone "blocked" {
    type master;
    file "/etc/bind/db.blocked";
    allow-query { localhost; };
};
```