#!/bin/bash

set -e

repo="https://raw.githubusercontent.com/StevenBlack/hosts/master/"
case $1 in
    "-fakenews-gambling-porn-social" )
        hostsfile="${repo}alternates/fakenews-gambling-porn-social/hosts";;
    "-fakenews-gambling" )
        hostsfile="${repo}alternates/fakenews-gambling/hosts";;
    "-gambling" )
        hostsfile="${repo}alternates/gambling/hosts";;
    "-fakenews" )
      hostsfile="${repo}alternates/fakenews/hosts";;
    "-porn" )
        hostsfile="${repo}alternates/porn/hosts";;
    "-social" )
        hostsfile="${repo}alternates/social/hosts";;
    "" )
        hostsfile="${repo}hosts";;
esac
echo $hostsfile
wget -q -O hosts $hostsfile

temp=$(mktemp)
temp_dedupe=$(mktemp)
db=$(mktemp)

cat hosts | grep '^0.0.0.0' | grep -E -v '127.0.0.1|255.255.255.255|::1' | cut -d " " -f 2 >> $temp
cat $temp | grep -E -v '^$|#' | sort | uniq  > $temp_dedupe

bind="/etc/bind/"
db_template="db.template"
db_empty="/etc/bind/db.empty"

if [ -f $db_template ]
then
    cat $db_template > $db
elif [ -f $db_empty ]
then
    cat $db_empty > $db
fi

cat $temp_dedupe | sed -r 's/(.*)/\1 CNAME ./' | grep -v "0.0.0.0 CNAME ." >> $db

sudo cp $db $bind/db.hosts

rm $db
rm $temp_dedupe
rm $temp
rm hosts

if ! [ -f $bind/db.allowed ]
then
    sudo cp bind/db.allowed $bind/db.allowed
    sudo tee -a "${bind}/named.conf" < bind/named.conf.allowed
fi

if ! [ -f $bind/db.blocked ]
then
    sudo cp bind/db.blocked $bind/db.blocked
    sudo tee -a "${bind}/named.conf" < bind/named.conf.blocked
fi

sudo tee -a "${bind}/named.conf" < bind/named.conf.hosts

sudo systemctl restart bind9



